//palceholder database: https://jsonplaceholder.typicode.com/posts
//fetch - used to perform crud operations in a given URL
	//accepts 2 arguments - URL(needed) & options (not a requirement)
		//options parameter - only used when dev needs the details of the request from the user(entering domething from the form)

//Get Post Data
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then((data) => {
	showPost(data);
	//showPost([data]);showPost instead of console.log to show typicode database
})

//Add Post
document.querySelector("#form-add-post").addEventListener('submit',(e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST",
		body: JSON.stringify({
			title:document.querySelector("#txt-title").value,
			body:document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers:{"Content-Type": "application/json; charset=UTF-8"}
	})
.then((response) => response.json()) //converts response to JSON format
.then(data => {
	console.log(data);
	alert("Post Created Successfully")

	//clears text in input fields upon creating post
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;
  	})
})


//Create showPost function that'll display created posts 

const showPost = (posts) =>{
	let postEntries = [];
	//forEach goes through each item one by one
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

//Edit Post Function

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	//removeAttribute - to remove the attribute from the element; receiuves a string argument that serves to be the attribute of the element
	document.querySelector("#btn-submit-update").removeAttribute('disabled')
}

//Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()
	fetch("https://jsonplaceholder.typicode.com/posts/1", { //parameter should be-- /:id
	method: 'PUT',
	body: JSON.stringify({
		id: document.querySelector("#txt-edit-id").value,
		title: document.querySelector("#txt-edit-title").value,
		body: document.querySelector("#txt-edit-body").value,
		userId: 1
	}),
	headers:{"Content-Type": "application/json; charset=UTF-8"}	
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Post Successfully Updated");

		//clears text in input fields upon creating post
	document.querySelector("#txt-edit-id").value = null;
	document.querySelector("#txt-edit-title").value = null;
	document.querySelector("#txt-edit-body").value = null;

	//setAttribute - sets the attribute of element; accepts 2 args:
		//string - attribute to be set
		//boolean - to set into true/false
	document.querySelector("#btn-submit-update").setAttribute('disabled', true)
	}) 
})

//ACTIVITY - make Delete button work
	//hint: could be a 3-line code

function deletePost(id) {
	const element = document.querySelector(`#post-${id}`);
	element.remove();
}